#include <unistd.h>
#include <sys/syscall.h>

int main()
{
  pid_t tid = syscall(SYS_tgkill);
  return 0;
}
