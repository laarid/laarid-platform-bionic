#include <unistd.h>
#include <sys/syscall.h>

#if defined(__APPLE__)
#define NR_GETTID SYS_thread_selfid
#else
#define NR_GETTID SYS_gettid
#endif

int main()
{
  pid_t tid = syscall(NR_GETTID);
  return 0;
}
