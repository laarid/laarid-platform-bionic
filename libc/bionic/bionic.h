/*
 * Copyright (C) 2014 You-Sheng Yang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __BIONIC_BIONIC_H__
#define __BIONIC_BIONIC_H__

#include <bionicconfig.h>

#include <sys/cdefs.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#if defined(__glibc_unlikely)
#define __predict_false(expr) __glibc_unlikely(expr)
#define __predict_true(expr)  __glibc_likely(expr)
#else
#define __predict_false(expr) (expr)
#define __predict_true(expr)  (expr)
#endif

#if !defined(__printflike)
#define __printflike(x, y) __attribute__((__format__(printf, x, y)))
#endif

#define __INTRODUCED_IN(api_level)

#define __LIBC_HIDDEN__ __attribute__((visibility("hidden")))

#define __BIONIC_ALIGN(__value, __alignment) (((__value) + (__alignment)-1) & ~((__alignment)-1))

#ifdef __cplusplus
extern "C" {
#endif

#if defined(BIONIC_NEED_GETTID)
pid_t gettid(void);
#endif

#if defined(BIONIC_NEED_TGKILL)
int tgkill(int tgid, int tid, int sig);
#endif

#if defined(BIONIC_NEED_GETPROGNAME)
const char* getprogname(void);
#endif

#if defined(BIONIC_NEED_STRLCAT)
size_t strlcat(char *__restrict dst, const char *__restrict src, size_t size);
#endif

#if defined(BIONIC_NEED_STRLCPY)
size_t strlcpy(char *__restrict dst, const char *__restrict src, size_t size);
#endif

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* __BIONIC_BIONIC_H__ */
